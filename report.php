#!/usr/bin/env php
<?php

declare(strict_types=1);

use foo\ReportGenerator;
use foo\view\ViewFactory;

require_once('vendor/autoload.php');

$reportGenerator = new ReportGenerator();

$report = $reportGenerator->generate();

$viewFactory = new ViewFactory(__DIR__ . '/view');

$mode = $_SERVER['argv'][1] ?? 'console';

$mode = $mode === 'html' ? $mode : 'console';

echo $viewFactory->render("$mode/report", ['report' => $report]);

