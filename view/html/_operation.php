<?php

declare(strict_types=1);

use foo\model\Operation;
use foo\view\ViewFactory;

/**
 * @var string $title
 * @var Operation $operation
 * @var ViewFactory $this
 */


?>

<div>
    Deposit: <br>
    <ul>
        <li>min: <?= $operation->getMin() ?></li>
        <li>max: <?= $operation->getMax() ?></li>
        <li><?= $this->render('html/_fee', ['fee' => $operation->getFee()]) ?></li>
    </ul>
</div>


