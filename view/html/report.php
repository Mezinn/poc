<?php

use foo\model\Report;
use foo\view\ViewFactory;

/**
 * @var Report $report
 * @var ViewFactory $this
 */

?>

<div>
    <h1>Report for <?= $report->getCreatedAt()->format('Y-m-d H:i:s') ?></h1>
    <?= $this->render('html/_operation', ['operation' => $report->getDeposit(), 'title' => 'Deposit']) ?>
    <?= $this->render('html/_operation', ['operation' => $report->getWithdraw(), 'title' => 'Withdraw']) ?>
</div>
