<?php

declare(strict_types=1);

namespace foo\view;

class ViewFactory
{
    private string $basePath;

    public function __construct(string $basePath)
    {
        $this->basePath = $basePath;
    }

    public function render($name, $payload = []): string
    {
        extract($payload);

        ob_start();

        require $this->basePath . '/' . $name . '.php';

        return ob_get_clean();
    }

}
