<?php

use foo\model\Report;
use foo\view\ViewFactory;

/**
 * @var Report $report
 * @var ViewFactory $this
 */

?>

+-------------------- Report for <?= $report->getCreatedAt()->format('Y-m-d H:i:s') ?> --------------------+
<?= $this->render('console/_operation', ['operation' => $report->getDeposit(), 'title' => 'Deposit']) ?>
<?= $this->render('console/_operation', ['operation' => $report->getWithdraw(), 'title' => 'Withdraw']) ?>
