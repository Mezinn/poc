<?php

declare(strict_types=1);

use foo\model\Operation;
use foo\view\ViewFactory;

/**
 * @var string $title
 * @var Operation $operation
 * @var ViewFactory $this
 */


?>

/------------------------------/
    <?=$title?>:

    min: <?= $operation->getMin() ?>

    max: <?= $operation->getMax() ?>

    <?= $this->render('console/_fee', ['fee' => $operation->getFee()]) ?>

/------------------------------/

