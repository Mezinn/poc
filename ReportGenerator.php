<?php

declare(strict_types=1);

namespace foo;

use DateTimeImmutable;
use foo\model\Deposit;
use foo\model\Fee;
use foo\model\Report;
use foo\model\Withdraw;

class ReportGenerator
{
    public function generate(): Report
    {
        return new Report(
            new Deposit(
                random_int(20, 100),
                random_int(200, 500),
                new Fee(
                    random_int(0, 10)
                )
            ),
            new Withdraw(
                random_int(20, 100),
                random_int(200, 500),
                new Fee(
                    random_int(0, 10)
                )
            ),
            new DateTimeImmutable()
        );
    }
}
