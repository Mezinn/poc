<?php

declare(strict_types=1);

namespace foo\model;

interface Operation
{
    public function getMin():int;
    public function getMax():int;
    public function getFee():Fee;
}
