<?php

declare(strict_types=1);

namespace foo\model;

class Fee
{
    private int $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
