<?php

declare(strict_types=1);

namespace foo\model;

use DateTimeImmutable;

class Report
{
    private Deposit $deposit;

    private Withdraw $withdraw;

    private DateTimeImmutable $createdAt;

    public function __construct(Deposit $deposit, Withdraw $withdraw, DateTimeImmutable $createdAt)
    {
        $this->deposit = $deposit;
        $this->withdraw = $withdraw;
        $this->createdAt = $createdAt;
    }

    public function getDeposit(): Deposit
    {
        return $this->deposit;
    }

    public function getWithdraw(): Withdraw
    {
        return $this->withdraw;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }
}
