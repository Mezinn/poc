<?php

declare(strict_types=1);

namespace foo\model;

class Deposit implements Operation
{
    private int $min;

    private int $max;

    private Fee $fee;

    public function __construct(int $min, int $max, Fee $fee)
    {
        $this->min = $min;
        $this->max = $max;
        $this->fee = $fee;
    }

    public function getMin(): int
    {
        return $this->min;
    }

    public function getMax(): int
    {
        return $this->max;
    }

    public function getFee(): Fee
    {
        return $this->fee;
    }
}
